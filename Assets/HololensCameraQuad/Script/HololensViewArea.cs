﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HololensViewArea : MonoBehaviour
{
    public void OnTriggerEnter(Collider other)
    {
        HololensViewAffectable affectable = other.GetComponent<HololensViewAffectable>();
        if (affectable != null)
            affectable.SetVisible(true);
    }
    public void OnTriggerExit(Collider other)
    {

        HololensViewAffectable affectable = other.GetComponent<HololensViewAffectable>();
        if (affectable != null)
            affectable.SetVisible(false);
    }


}

public interface HololensViewAffectable
{

    void SetVisible(bool value);

}