﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundRandomGroup : MonoBehaviour
{
    public AudioSource m_source;
    public List<AudioClip> m_sounds;

   
    public void PlayRandom() {
        m_source.clip = m_sounds[Random.Range(0, m_sounds.Count)];
        m_source.Play();

    }

}
