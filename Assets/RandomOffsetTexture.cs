﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomOffsetTexture : MonoBehaviour
{
    public Renderer m_renderer;
    public float m_rotationSpeed=1;
    public Vector2 m_direction;
    void Start()
    {
        m_direction.x = UnityEngine.Random.Range(-1f, 1f);
        m_direction.y = UnityEngine.Random.Range(-1f, 1f);
    }
    void Update()
    {
        m_renderer.material.SetTextureOffset("_MainTex", m_direction * Time.deltaTime * m_rotationSpeed);
    }
}
