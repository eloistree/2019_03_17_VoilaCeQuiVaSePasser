﻿using EloiExperiments.Toolbox.RandomTool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RandomCreator : MonoBehaviour
{

    public GameObject[] m_prefabs;
    public int minCount=1, maxCount=4;
    IEnumerator  Start()
    {
        for (int i = 0; i < RandomTool.GetInteger(minCount, maxCount); i++)
        {
            yield return new WaitForEndOfFrame();
            CreatePrefab();

        }
    }

    public GameObject CreatePrefab( )
    {
        if (m_prefabs.Length <= 0) return null;
        Vector3 pos; Quaternion rot;
        GetRandomPosition(out pos, out rot);
        GameObject obj = Instantiate(m_prefabs[RandomTool.GetInteger(0, m_prefabs.Length - 1)],pos,rot);
  
        return obj;
    }

    public abstract void GetRandomPosition(out Vector3 position, out Quaternion rotation);
}
