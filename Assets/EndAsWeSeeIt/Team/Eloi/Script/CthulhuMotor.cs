﻿using Rewired;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class CthulhuMotor : MonoBehaviour
    {


    [Header("Speed & Rotation")]
        public Transform m_affectedBody;
        public Vector2 m_bodyMoveSpeed= new Vector2(1,1);
        public Transform m_affectedHead;
        public Vector2 m_headRotationSpeed = new Vector2(180,90);


    [Header("Foot Position")]
    public Transform m_leftFootPosition;
    public Transform m_rightFootPosition;
    public float m_offsetMoveForward = 4;
    public float m_offsetMoveBackward = 2;

    [Header("Event")]
    public UnityEvent m_onLeftSplashRequested;
    public UnityEvent m_onRightSplashRequested;


    public int playerId;
        private Player player;
        [SerializeField] string splashRight = "SplashRight";
        [SerializeField] string splashLeft = "SplashLeft";
        [SerializeField] string moveForward = "MoveForward";
        [SerializeField] string moveSide = "MoveSide";
        [SerializeField] string restart  = "Restart";
        [SerializeField] string moveHorizontalHead = "MoveHorizontalHead";
        [SerializeField] string moveVerticalHead = "MoveVerticalHead";
        [SerializeField] string escape = "Escape";


  


public void DoWhenReInputIsNull()
    {
        if (ReInput.players == null)
            return;
        // Get the Player for a particular playerId
        player = ReInput.players.GetPlayer(playerId);

       
    }

    [SerializeField] Vector3 m_move;
        [SerializeField] float m_rotateHorizontal;
        [SerializeField] float m_rotateVertical;
    private void Awake ()
    {
        player = null;
    }
    private void OnLevelWasLoaded(int level)
    {
        player = null;
    }

    private void Update()
    {
        if (player == null)
            DoWhenReInputIsNull();
        if (player == null)
            return;

        m_move.x = player.GetAxis(moveSide);
            m_move.z = player.GetAxis(moveForward);
       

            m_move.x *= m_bodyMoveSpeed.x;
            m_move.z *= m_bodyMoveSpeed.y;

        m_rotateHorizontal = player.GetAxis(moveHorizontalHead) * m_headRotationSpeed.x * Time.deltaTime;
        m_rotateVertical += -player.GetAxis(moveVerticalHead) * m_headRotationSpeed.y * Time.deltaTime;
        m_rotateVertical = Mathf.Clamp(m_rotateVertical, -90f, 90f);

            m_affectedBody.Translate(m_move * Time.deltaTime, Space.Self);
            m_affectedBody.Rotate(new Vector3(0, m_rotateHorizontal,0), Space.Self);
            m_affectedHead.localRotation = Quaternion.Euler(new Vector3(m_rotateVertical,0,0));

        if (player.GetButton(splashLeft) || IsVrPressingLeft())
            m_onLeftSplashRequested.Invoke();
        if (player.GetButton(splashRight) || IsVrPressingRight())
            m_onRightSplashRequested.Invoke();


        if (m_leftFootPosition == null)
        {
            GameObject obj = GameObject.FindWithTag("LeftFoot");
            if (obj != null)
                m_leftFootPosition = obj.transform;
        }
        if (m_rightFootPosition == null)
        {
            GameObject obj = GameObject.FindWithTag("RightFoot");
            if (obj != null)
                m_rightFootPosition = obj.transform;
        }

        if (m_leftFootPosition && m_rightFootPosition) { 
            float moveFor = player.GetAxis(moveForward);
            float adjustementZ = moveFor > 0 ?
                m_offsetMoveForward * moveFor : m_offsetMoveBackward * moveFor;
            Vector3 lateralAdujustement = m_leftFootPosition.localPosition;
            lateralAdujustement.z = adjustementZ;
            m_leftFootPosition.localPosition = lateralAdujustement;

             lateralAdujustement = m_rightFootPosition.localPosition;
            lateralAdujustement.z = adjustementZ;
            m_rightFootPosition.localPosition = lateralAdujustement;
        }
    }

    private bool IsVrPressingLeft()
    {
        return Input.GetAxis("MRTriggerLeft") != 0f;
    }
    private bool IsVrPressingRight()
    {
        return Input.GetAxis("MRTriggerRight") != 0f;
    }
}

