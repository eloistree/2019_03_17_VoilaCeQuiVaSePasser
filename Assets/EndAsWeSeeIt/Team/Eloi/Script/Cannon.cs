﻿using EloiExperiments.Toolbox.RandomTool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public Transform m_anchor;
    public CreatePrefab m_bulletCreator;
    public float m_minTime = 1, m_maxTime = 10;
    public Transform m_target;

    private IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForSeconds(RandomTool.GetFloat(m_minTime, m_maxTime));
            CreateBullet();

        }
    }

    private void CreateBullet()
    {
        m_bulletCreator.Create();
    }

    void Update()
    {
        if (m_target == null)
        {
            CannonTargetQuickTag tag = GameObject.FindObjectOfType<CannonTargetQuickTag>();
            if (tag)
                m_target = tag.transform;
        }
        if (m_target == null)
            m_target = Camera.main.transform;
        if (m_target)
        {
            m_anchor.LookAt(m_target);

        }


    }
}
