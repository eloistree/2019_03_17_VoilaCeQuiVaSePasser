﻿using EloiExperiments.Toolbox.RandomTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCreateCircle : RandomCreator
{
    public Transform m_where;
    public float m_minRadius = 1;
    public float m_maxRadius = 5;
    public override void GetRandomPosition(out Vector3 position, out Quaternion rotation)
    {
        float angle = RandomTool.GetFloat(360, false);
        position = m_where.position + RandomTool.GetFloat(m_minRadius, m_maxRadius) *(m_where.rotation *new Vector3(Mathf.Sin(angle), 0, Mathf.Cos(angle))) ;
        rotation = m_where.rotation * Quaternion.Euler(0,RandomTool.GetFloat(360, false),0);
    }

}
