﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBodyPart : MonoBehaviour
{

    public Transform m_body;
    public Transform m_leftFoot;
    public Transform m_rightFoot;
    public Transform m_leftHand;
    public Transform m_rightHand;

 
    void LateUpdate()
    {

              if (VRBodyPart.m_head!=null) {
        Vector3 newBodyPosition = VRBodyPart.m_head.transform.position;
            m_body.transform.position = new Vector3(newBodyPosition.x, m_body.transform.position.y, newBodyPosition.z);
            m_body.transform.rotation = Quaternion.Euler( new Vector3(0, VRBodyPart.m_head.transform.eulerAngles.y, 0));

        }
        if (VRBodyPart.m_leftHand) {
            m_leftHand.rotation = VRBodyPart.m_leftHand.transform.rotation;
            m_leftHand.position = VRBodyPart.m_leftHand.transform.position;
            if (VRBodyPart.m_head)

                Debug.DrawLine(m_leftFoot.position, VRBodyPart.m_head.transform.position, Color.cyan);
        }

        if (VRBodyPart.m_rightHand) {
            m_rightHand.position = VRBodyPart.m_rightHand.transform.position;
            m_rightHand.rotation = VRBodyPart.m_rightHand.transform.rotation;
            if(VRBodyPart.m_head)
                Debug.DrawLine(m_rightFoot.position, VRBodyPart.m_head.transform.position, Color.cyan);

        }



    }
}
