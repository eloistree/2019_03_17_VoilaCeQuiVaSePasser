﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRBodyPart : MonoBehaviour
{
    public Transform m_affected;
    public Part m_part;
    public enum Part { Head, LeftHand, RightHand}

    public static VRBodyPart m_head;
    public static VRBodyPart m_leftHand;
    public static VRBodyPart m_rightHand;

    void OnEnable()
    {
        if (m_part == Part.Head)
            m_head = this;
        if (m_part == Part.LeftHand)
            m_leftHand = this;
        if (m_part == Part.RightHand)
            m_rightHand = this;
    }
    void OnDisable()
    {


    }
    private void Reset()
    {
        m_affected = transform;
    }
}
