﻿using EloiExperiments.Toolbox.RandomTool;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomChangeDirection : MonoBehaviour
{
    public Transform m_affected;
    public float m_minTime=4;
    public float m_maxTime=10;

    void Start()
    {
        ChangeDirection();
    }

    private void ChangeDirection()
    {
        m_affected.Rotate(
            new Vector3(0, RandomTool.GetFloat(0, 360), 0),
            Space.World);
        Invoke("ChangeDirection",RandomTool.GetFloat(m_minTime, m_maxTime));
    }
    public void Reset()
    {
        m_affected = this.transform;
    }
}
