﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CrushByLooking : MonoBehaviour
{
    public Transform m_origine;
    public Transform m_direction;
    public LayerMask m_curshable;
    public float m_sphereCastRadius = 0.5f;
    public UnityEvent m_crusahbleInSight;

    public Collider m_lastTarget;
    public void Update()
    {
        UseMainCameraIfNone();

        RaycastHit hit;
        bool hasHit = Physics.SphereCast(m_origine.position, m_sphereCastRadius, m_origine.forward, out hit, float.MaxValue, m_curshable);
        if (hasHit)
        {
            if (m_lastTarget != hit.collider)
            {
                m_lastTarget = hit.collider;
                m_crusahbleInSight.Invoke();
            }
        }
        else m_lastTarget = null;


    }

    private void UseMainCameraIfNone()
    {
        if (Camera.main)
        {
            if (m_origine == null)
                m_origine = Camera.main.transform;

            if (m_direction == null)
                m_direction = Camera.main.transform;
        }
    }

    private float GetDistanceOfRoot(Vector3 point)
    {
        Vector3 camera = m_origine.position;
        camera.y = 0;
        point.y = 0;
        return Vector3.Distance(camera, point);
    }

    private void Reset()
    {
        m_origine = transform;
        m_direction = transform;
    }
}
