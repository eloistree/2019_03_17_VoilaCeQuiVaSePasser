﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IsFootableRaycast : MonoBehaviour
{
    public Transform m_origine;
    public Transform m_direction;
    public LayerMask m_ground;
    public float m_maxDistance= 2;
    public Transform m_cursorDebug;

    [Header("Debug")]
    public bool m_isHittingGround;
    public Vector3 m_cursorPosition;
    public Collider m_collision;

    public void Update()
    {
        UseMainCameraIfNone();

        RaycastHit hit;
        bool hasHit = Physics.Raycast(m_origine.position, m_origine.forward, out hit, float.MaxValue, m_ground);
        float distanceOfFootCameraRoot = GetDistanceOfRoot(hit.point);
        if (distanceOfFootCameraRoot <= m_maxDistance) 
        {
            Debug.DrawLine(m_origine.position, hit.point, Color.green);
            m_cursorPosition = hit.point;
            if (m_cursorDebug) {
                m_cursorDebug.position = m_cursorPosition;
            }
        }
        else
        {
            Vector3 destination = m_origine.position + m_origine.forward * m_maxDistance;
            Debug.DrawLine(m_origine.position,destination, Color.red);
            m_cursorPosition = destination;
            if (m_cursorDebug)
            {
                m_cursorDebug.position = m_cursorPosition;
            }
        }

        m_isHittingGround = hasHit;
        m_collision = hit.collider;
    }

    private void UseMainCameraIfNone()
    {
        if (Camera.main) {
            if(m_origine==null)
                m_origine = Camera.main.transform;

            if (m_direction == null)
                m_direction = Camera.main.transform;
        }
    }

    private float GetDistanceOfRoot(Vector3 point)
    {
        Vector3 camera = m_origine.position;
        camera.y = 0;
        point.y = 0;
        return Vector3.Distance(camera, point);
    }

    private void Reset()
    {
        m_origine = transform;
        m_direction = transform;
    }
}
