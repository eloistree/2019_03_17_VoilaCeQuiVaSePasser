﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetCrushManager : MonoBehaviour
{
    public CrushManager m_leftFoot;
    public CrushManager m_rightFoot;


    public void RandomCrush()
    {
        if (Time.time % 2 == 0)
            m_leftFoot.Crush();
        else m_rightFoot.Crush();
    }
    public void UseCloseCrush()
    {

    }
    
}
