﻿using EloiExperiments.Toolbox.RandomTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomCreateCube : RandomCreator
{
    public Transform m_where;

    public override void GetRandomPosition(out Vector3 position, out Quaternion rotation)
    {
        position = m_where.position + (m_where.rotation*  new Vector3(
            RandomTool.GetFloat(m_where.localScale.x*0.5f, true),
            RandomTool.GetFloat(m_where.localScale.y * 0.5f, true),
            RandomTool.GetFloat(m_where.localScale.z * 0.5f, true)
            )) ;    
        rotation = m_where.rotation * Quaternion.Euler(0, RandomTool.GetFloat(360, false), 0);
    }

}
