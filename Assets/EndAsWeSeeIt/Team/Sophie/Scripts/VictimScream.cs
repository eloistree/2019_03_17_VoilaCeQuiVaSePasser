﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictimScream : MonoBehaviour
{
    public GameObject audioSource;
    public float maxAudioDistance = 7;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(transform.position, Camera.main.transform.position) > maxAudioDistance)
        {
            audioSource.SetActive(false);
        }
        else
        {
            audioSource.SetActive(true);
        }
    }
}
