﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CrushManager : MonoBehaviour
{
    public Animator animator;
    public IsFootableRaycast isFootableRaycast;
    public Transform camera;
    public Transform anchor;
    public float crushTime = 0.4f;
    public float stepTime = 1;
    public float maxCameraDistance = 1;
    public float urgentMaxCameraDistance = 2;
    public bool useDebug;

    private float startTime;
    private float moveTime;
    private bool isStepping;
    private Vector3 target;
    
    public static float m_cooldown;
    public static float m_cooldownDuration =2.8f;
    public float m_timeNoAction;
    public float m_maxTimeBetweenAction=5;
    public UnityEvent m_onFootDown;
    public UnityEvent m_onCrushDown;
    public enum StepType { Step, Crush}
    public StepType m_stepType;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (camera == null && Camera.main!=null ) {
            camera = Camera.main.transform;
        }
        m_timeNoAction += Time.deltaTime;
        if (m_cooldown > 0)
        {
            m_cooldown -= Time.deltaTime;
            if (m_cooldown < 0)
                m_cooldown = 0f;
        }
        if (m_timeNoAction > m_maxTimeBetweenAction) {
            m_timeNoAction = 0;
            Step();
        }

        if (useDebug && Input.GetMouseButtonDown(0))
        {
            Crush();
        }
        if (useDebug && Input.GetMouseButtonDown(1))
        {
            Step();
        }

        if (isStepping)
        {
            float stepFraction = (Time.time - startTime) / moveTime;
            if (stepFraction < 1)
            {
                transform.SetPositionAndRotation(Vector3.Lerp(transform.position,target,stepFraction), Quaternion.Lerp(transform.rotation, GetHorizontalCameraDirection(), stepFraction));
            }
            else
            {
                m_cooldown = 0f;
                isStepping = false;
                if (m_stepType == StepType.Step)
                    m_onFootDown.Invoke();
                else m_onCrushDown.Invoke();
                transform.SetPositionAndRotation(target, GetHorizontalCameraDirection());
            }
          //  print("Pos :" + transform.position + " Target : " + target);
        }
        else
        {
            
            if (IsOutOfCameraDistance( maxCameraDistance))
            {
                Step();
            }
        }
    }

    private bool IsOutOfCameraDistance( float distance)
    {
        Vector3 camRelativePos = camera.position;
        camRelativePos.y = transform.position.y;
        return GetCameraDistance(camRelativePos) > distance;
    }

    private float GetCameraDistance(Vector3 camRelativePos)
    {
        return Vector3.Distance(camRelativePos, transform.position);
    }

    private Quaternion GetHorizontalCameraDirection()
    {
        Vector3 euler =
        camera.transform.eulerAngles;
        euler.x = 0;
        euler.z = 0;
        return Quaternion.Euler(euler);

    }

    public  void Crush()
    {
        if (m_cooldown>0f && !IsOutOfCameraDistance(urgentMaxCameraDistance)) return;
        if (isFootableRaycast.m_isHittingGround && !isStepping)
        {
            m_stepType = StepType.Crush;
               m_cooldown = m_cooldownDuration;
            isStepping = true;
            startTime = Time.time;
            moveTime = crushTime;
            target = isFootableRaycast.m_cursorPosition;
            target.y = transform.position.y;
            animator.SetTrigger("crush");
        }

    }

    public void Step ()
    {
        if (m_cooldown > 0f && !IsOutOfCameraDistance(urgentMaxCameraDistance)) return;
        m_stepType = StepType.Step;
        m_cooldown = m_cooldownDuration;
        isStepping = true;
        startTime = Time.time;
        moveTime = stepTime;
        target = anchor.position;
        animator.SetTrigger("step");
    }
}
