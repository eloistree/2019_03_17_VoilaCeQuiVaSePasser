﻿using EloiExperiments.Toolbox.RandomTool;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : RandomCreator
{
    public Transform m_where;
    public bool respawning;
    public float firstRespawnTime = 30;
    public float reduceTimeRespawn = 5;
    public float minRespawnTime = 5;
    public int maxSpawn = 10;

    private float timeCounter;
    private float respawnTime;
    private List<GameObject> spawnedObjects = new List<GameObject>();

    public override void GetRandomPosition(out Vector3 position, out Quaternion rotation)
    {
        position = m_where.position + (m_where.rotation * new Vector3(
            RandomTool.GetFloat(m_where.localScale.x * 0.5f, true),
            RandomTool.GetFloat(m_where.localScale.y * 0.5f, true),
            RandomTool.GetFloat(m_where.localScale.z * 0.5f, true)
            ));
        rotation = m_where.rotation * Quaternion.Euler(0, RandomTool.GetFloat(360, false), 0);
    }

    private void Start()
    {
        respawnTime = firstRespawnTime;
    }

    private void Update()
    {
        if (respawning)
        {
            for (int i = 0; i < spawnedObjects.Count; i++)
            {
                if (spawnedObjects[i] == null)
                {
                    spawnedObjects.Remove(spawnedObjects[i]);
                }
            }
            timeCounter += Time.deltaTime;
            if (timeCounter >= respawnTime)
            {
                if (spawnedObjects.Count < maxSpawn)
                {
                    spawnedObjects.Add(CreatePrefab());
                    timeCounter = 0;
                    respawnTime = Mathf.Max(minRespawnTime, respawnTime - reduceTimeRespawn);
                }
            }
        }
    }
}
