﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Multiply : MonoBehaviour
{
    public GameObject model;

    private int counter = 0;
    private Vector3 position;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            counter = (counter + 1) % 4;
            position.x += (counter % 2 == 0 ? 0 : 6);
            position.z += (counter % 2 == 0 ? 6 : 0);
            position.x *= (counter < 2 ? 1 : -1);
            position.z *= (counter < 2 ? 1 : -1);
            GameObject.Instantiate(model,position,Quaternion.identity);
            print(counter);
        }
    }
}
