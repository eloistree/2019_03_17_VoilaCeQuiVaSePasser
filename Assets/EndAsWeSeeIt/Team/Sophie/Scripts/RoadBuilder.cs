﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBuilder : MonoBehaviour
{
    public GameObject model;
    public Transform startPos;
    public Transform endPos;

    public float roadDistance = 5;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 pos = startPos.position;
        float stepX = 0;
        if (startPos.position.x > endPos.position.x)
        {
            stepX = -1 * roadDistance;
        }
        else if (startPos.position.x < endPos.position.x)
        {
            stepX = roadDistance;
        }
        float stepZ = 0;
        if (startPos.position.z > endPos.position.z)
        {
            stepZ = -1 * roadDistance;
        }
        else if (startPos.position.z < endPos.position.z)
        {
            stepZ = roadDistance;
        }
        while (Mathf.Abs(pos.x - endPos.position.x) > roadDistance || Mathf.Abs(pos.z - endPos.position.z) > roadDistance)
        {
            GameObject.Instantiate(model, pos, new Quaternion(), transform);
            pos.x += stepX;
            pos.z += stepZ;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
